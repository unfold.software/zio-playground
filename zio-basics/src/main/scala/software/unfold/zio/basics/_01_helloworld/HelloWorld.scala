package software.unfold.zio.basics._01_helloworld

import zio.Console.{printLine, readLine}
import zio.{ZIO, ZIOAppDefault}

import java.io.IOException

object HelloWorld extends ZIOAppDefault {
  override def run: ZIO[Any, IOException, Unit] = for {
    _ <- printLine("Hello, what's your name?")
    name <- readLine
    _ <- printLine(s"Hello, ${name}!")
  } yield ()
}
