ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "3.1.3"

lazy val `zio-basics` = project
  .settings(
    libraryDependencies += "dev.zio" %% "zio" % "2.0.0"
  )

lazy val root = (project in file("."))
  .settings(
    name := "zio-playground",
  ).aggregate(
    `zio-basics`
)
